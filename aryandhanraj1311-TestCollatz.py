#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

        s2 = "10 1\n"
        i, j = collatz_read(s2)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

        s3 = "501 699\n"
        i, j = collatz_read(s3)
        self.assertEqual(i,  501)
        self.assertEqual(j, 699)

    def test_read_1(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)

        s2 = "200 100\n"
        i, j = collatz_read(s2)
        self.assertEqual(i,  200)
        self.assertEqual(j, 100)

        s3 = "10001 20000\n"
        i, j = collatz_read(s3)
        self.assertEqual(i,  10001)
        self.assertEqual(j, 20000)
    
    def test_read_2(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 210)

        s2 = "210 201\n"
        i, j = collatz_read(s2)
        self.assertEqual(i, 210)
        self.assertEqual(j, 201)

        s3 = "11 27\n"
        i, j = collatz_read(s3)
        self.assertEqual(i,  11)
        self.assertEqual(j, 27)
    
    def test_read_3(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  900)
        self.assertEqual(j, 1000)

        s2 = "1000 900\n"
        i, j = collatz_read(s2)
        self.assertEqual(i,  1000)
        self.assertEqual(j, 900)

        s3 = "27 101\n"
        i, j = collatz_read(s3)
        self.assertEqual(i,  27)
        self.assertEqual(j, 101)

    # ----
    # eval
    # ----

    

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)
        
        x = collatz_eval(10, 1)
        self.assertEqual(x, 20)

        y = collatz_eval(501, 699)
        self.assertEqual(y, 145)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

        x = collatz_eval(200, 100)
        self.assertEqual(x, 125)

        y = collatz_eval(10001, 20000)
        self.assertEqual(y, 279)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

        x = collatz_eval(210, 201)
        self.assertEqual(x, 89)

        y = collatz_eval(11, 27)
        self.assertEqual(y, 112)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

        x = collatz_eval(1000, 900)
        self.assertEqual(x, 174)

        y = collatz_eval(27, 101)
        self.assertEqual(y, 119)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

        x = StringIO()
        collatz_print(x, 10, 1, 20)
        self.assertEqual(x.getvalue(), "10 1 20\n")

        y = StringIO()
        collatz_print(y, 501, 699, 145)
        self.assertEqual(y.getvalue(), "501 699 145\n")
    
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

        x = StringIO()
        collatz_print(x, 200, 100, 125)
        self.assertEqual(x.getvalue(), "200 100 125\n")

        y = StringIO()
        collatz_print(y, 10001, 20000, 279)
        self.assertEqual(y.getvalue(), "10001 20000 279\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

        x = StringIO()
        collatz_print(x, 210, 201, 89)
        self.assertEqual(x.getvalue(), "210 201 89\n")

        y = StringIO()
        collatz_print(y, 11, 27, 112)
        self.assertEqual(y.getvalue(), "11 27 112\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

        x = StringIO()
        collatz_print(x, 1000, 900, 174)
        self.assertEqual(x.getvalue(), "1000 900 174\n")

        y = StringIO()
        collatz_print(y, 27, 101, 119)
        self.assertEqual(y.getvalue(), "27 101 119\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n10 1\n501 699\n100 200\n200 100\n10001 20000\n201 210\n210 201\n11 27\n900 1000\n1000 900\n27 101\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n10 1 20\n501 699 145\n100 200 125\n200 100 125\n10001 20000 279\n201 210 89\n210 201 89\n11 27 112\n900 1000 174\n1000 900 174\n27 101 119\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
